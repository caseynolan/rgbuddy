//
//  ViewController.swift
//  RGBuddy
//
//  Created by Casey Nolan on 3/12/2015.
//  Copyright © 2015 Casey Nolan. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {

    // MARK: - Properties
    // The properties to hold individual color values, initialised to 255 for white
    var redValue = 255
    var greenValue = 255
    var blueValue = 255
    
    
    // MARK: - Storyboard
    
    @IBOutlet weak var hexTextField: UITextField!
    
    // MARK: Red
    @IBOutlet weak var redStepper: UIStepper!
    @IBAction func redStepper(sender: UIStepper) {
        redValue = Int(sender.value)
        updateValues()
    }
    
    @IBOutlet weak var redSlider: UISlider!
    @IBAction func redSlider(sender: UISlider) {
        redValue = Int(sender.value)
        updateValues()
    }
    
    @IBOutlet weak var redTextField: UITextField!
    
    
    // MARK: Green
    @IBOutlet weak var greenStepper: UIStepper!
    @IBAction func greenStepper(sender: UIStepper) {
        greenValue = Int(sender.value)
        updateValues()
    }
    
    @IBOutlet weak var greenSlider: UISlider!
    @IBAction func greenSlider(sender: UISlider) {
        greenValue = Int(sender.value)
        updateValues()
    }    
    
    @IBOutlet weak var greenTextField: UITextField!
    
    
    // MARK: Blue
    @IBOutlet weak var blueStepper: UIStepper!
    @IBAction func blueStepper(sender: UIStepper) {
        blueValue = Int(sender.value)
        updateValues()
    }
    
    @IBOutlet weak var blueSlider: UISlider!
    @IBAction func blueSlider(sender: UISlider) {
        blueValue = Int(sender.value)
        updateValues()
    }

    @IBOutlet weak var blueTextField: UITextField!
    
    
    // MARK: - Custom Methods
    func updateValues() {
        redTextField.text = String(redValue)
        redSlider.value = Float(redValue)
        redStepper.value = Double(redValue)
        
        greenTextField.text = String(greenValue)
        greenSlider.value = Float(greenValue)
        greenStepper.value = Double(greenValue)
        
        blueTextField.text = String(blueValue)
        blueSlider.value = Float(blueValue)
        blueStepper.value = Double(blueValue)
        
        hexTextField.text = hexValueFromRGB(redValue, green: greenValue, blue: blueValue)
        
        self.view.backgroundColor = UIColor(red: CGFloat(Double(redValue) / 255.0), green: CGFloat(Double(greenValue) / 255.0), blue: CGFloat(Double(blueValue) / 255.0), alpha: 1.0)
        
    }
    
    func hexValueFromRGB(red: Int, green: Int, blue: Int) -> String {
        
        let redHex = NSString(format: "%.02X", red)
        let greenHex = NSString(format: "%.02X", green)
        let blueHex = NSString(format: "%.02X", blue)

        return String("#" + (redHex as String) + (greenHex as String) + (blueHex as String))
    }
    
    
    // MARK: - UIViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.updateValues() // Force a fresh display
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - UIResponder
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        // The keyboard will be dismissed when a user taps outside the UITextField
        self.view.endEditing(true)
    }
    
    
    // MARK: - UITextFieldDelegate
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        // This will tell the text field to close whenever the Return button is tapped
        textField.resignFirstResponder()
        return true
    }


}

